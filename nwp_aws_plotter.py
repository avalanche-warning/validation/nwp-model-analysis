#! /usr/bin/python3
################################################################################
# Copyright 2022 Avalanche Warning Service Tyrol                               #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################

import os
from datetime import datetime, timedelta, date
import subprocess
import glob

import numpy as np
import pandas as pd
from scipy import stats, optimize


import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import AutoMinorLocator
from matplotlib.colors import BoundaryNorm
from matplotlib.projections import PolarAxes
import mpl_toolkits.axisartist.grid_finder as gf
import mpl_toolkits.axisartist.floating_axes as fa

if os.path.exists('latex_default.mplstyle'):
    plt.style.use('latex_default.mplstyle')


def _download_sp_stations(stations: list, sdate: str, edate: str, aws_data_dir: str, download_script_path) -> list:
    """Prepare list of stations to run and download data for them."""
    data_src = "lwdt"
    if data_src == "lwdt":
        for station in stations:
            #output_dir = get_dl_path(domain)
            #stat_file = os.path.join(get_dl_path(domain), station + ".smet")
            data_span = f"{sdate}/{edate}" # date format of lwdt API

            subprocess.call(["python3", download_script_path, station, data_span, aws_data_dir])

        # check if there are smets from older setups which are not needed for runs like current one:
        all_smets = glob.glob(os.path.join(aws_data_dir, "*.smet"))
        for smet in all_smets:
            stat = os.path.basename(os.path.splitext(smet)[0])
            if not stat in stations:
                print(f'[i] Removing rogue data file for station "{stat}"')
                os.remove(smet)
        try:
            os.rmdir(os.path.join(aws_data_dir, "tmp"))
        except OSError:
            pass
    else:
        print("[E] Module for data source {data_src} not configured.")
        exit(1)


def plot_timeseries(ds_aro, ds_ifs, station_info, df_aws=None, sdate=None, edate=None):
    """Timeseries plots of relevant parameters similar to weather station plots"""

    """Prepare data / Select location"""
    ds_aro0 = ds_aro.sel(x=station_info['easting'],y=station_info['northing'],method='nearest')
    ds_ifs0 = ds_ifs.sel(x=station_info['easting'],y=station_info['northing'],method='nearest')
    
    sdate_dt      = datetime.strptime(sdate, "%Y-%m-%d")
    edate_dt      = datetime.strptime(edate, "%Y-%m-%d")
    edate_dt_sel  = edate_dt - timedelta(days=1)
    edate_sel     = edate_dt_sel.strftime("%Y-%m-%d")
    ds_aro0 = ds_aro0.sel(time=slice(sdate,edate_sel))
    ds_ifs0 = ds_ifs0.sel(time=slice(sdate,edate_sel))
    df_aws  = df_aws.set_index("time", drop=False)
    df_aws0 = df_aws[sdate_dt:edate_dt]
    # df_aws0 = df_aws[df_aws['time'].isin(ds_aro0.time.values)]

    ds_aro0['PCUMSUM'] = ds_aro0['PSUM'].cumsum()
    ds_ifs0['PCUMSUM'] = ds_ifs0['PSUM'].cumsum()

    gskw = {'hspace':0}
    fig, axes = plt.subplots(6,1,figsize=(8,12),sharex=True, gridspec_kw=gskw)
    COLS=['black', 'blueviolet', 'goldenrod', 'firebrick', 'forestgreen', 'steelblue', 'olive', 'peru']

    caws = 'peru' # 'orangered'
    caro = 'forestgreen'
    cifs = 'blueviolet'

    label_aro = 'AROME (NWP)'
    label_ifs = 'IFS (NWP)'
    label_aws = 'AWS (Measurement)'

    ls_aro = '--'
    ls_ifs = '--'
    ls_aws = '-'

    m_nwp = 'o'
    m_aws = 'x'
    m_size  = 1.5
    l_width = 1.75
    lw_aws  = 0.7
    m_alpha = 0.67
    
    """TA"""
    if df_aws is not None:
        axes[0].plot(df_aws0.time, df_aws0.TA, color=caws, lw=lw_aws, ls=ls_aws, label=label_aws)
    axes[0].plot(ds_aro0.time, ds_aro0.TA, color=caro, lw=l_width, ls=ls_aro, label=label_aro)
    axes[0].plot(ds_ifs0.time, ds_ifs0.TA, color=cifs, lw=l_width, ls=ls_ifs, label=label_ifs)
    # markerfacecolor='none', marker=m_nwp, ms=m_size, alpha=m_alpha
    axes[0].set_ylabel('Temperature / °C')
    axes[0].set_ylim([-16,21])

    axes[0].xaxis.set_label_position('top')
    axes[0].xaxis.set_ticks_position('top')
    axes[0].legend(loc='upper right', ncol=1,  framealpha=1, edgecolor='black') # 'upper left' # prop={'size': 10}

    """RH"""
    if df_aws is not None:
        axes[1].plot(df_aws0.time, df_aws0.RH, color=caws, lw=lw_aws, ls=ls_aws)
    axes[1].plot(ds_aro0.time, ds_aro0.RH, color=caro, lw=l_width, ls=ls_aro)
    axes[1].plot(ds_ifs0.time, ds_ifs0.RH, color=cifs, lw=l_width, ls=ls_ifs)
    axes[1].set_ylabel('Humidity / %')
    axes[1].set_ylim([8,105])

    """VW and DW"""
    if df_aws is not None:
        axes[2].plot(df_aws0.time, df_aws0.VW, color=caws, lw=lw_aws, ls=ls_aws)
    # # axes[2].scatter(ds_aws.time.values, ds_aws.WG.values, color=caws, s=20, marker="X", label='AWS')
    axes[2].plot(ds_aro0.time, ds_aro0.VW, color=caro, lw=l_width, ls=ls_aro)
    axes[2].plot(ds_ifs0.time, ds_ifs0.VW, color=cifs, lw=l_width, ls=ls_ifs)
    axes[2].set_ylabel(r'Wind speed / ms$^{-1}$')
    axes[2].set_ylim([-1,14])
    # ax_dw = axes[2].twinx()
    # ax_dw.scatter(ds_aro0.time.values, ds_aro0.DW.values, color=caro, alpha=0.3, s=m_size, marker=m_nwp)
    # ax_dw.scatter(ds_ifs0.time.values, ds_ifs0.DW.values, color=cifs, alpha=0.3, s=m_size, marker=m_nwp)
    # ax_dw.set_ylabel('Wind direction / °')
    # ax_dw.set_ylim([-10,370])
    # ax_dw.xaxis.set_minor_locator(AutoMinorLocator())
    # ax_dw.yaxis.set_minor_locator(AutoMinorLocator())
    # ax_dw.grid(False)

    """PSUM"""
    if df_aws0 is not None:
        if "PSUM" in df_aws0.columns:
            wbar = df_aws0.time[10]-df_aws0.time[9]
            tres = wbar.seconds // 60
            df_aws0.loc[:,'PSUM_scaled'] = df_aws0['PSUM'].rolling(window=int(60/tres)).sum()
            #axes[3].bar(df_aws.time, df_aws.PSUM, width=wbar, color=caws, alpha=0.3)
            axes[3].bar(df_aws0.time, df_aws0.PSUM_scaled, width=wbar, color=caws, alpha=0.3)
        # axes[3].plot(ds_aws.time, ds_aws.N, color=caws, lw=0.75, ls='--', marker='2', alpha=0.75, label='AWS (Measurements)')
    wbar = ds_ifs0.time[1]-ds_ifs0.time[0]
    axes[3].bar(ds_aro0.time, ds_aro0.PSUM, width=wbar, color=caro, alpha=0.3)
    axes[3].bar(ds_ifs0.time, ds_ifs0.PSUM, width=wbar, color=cifs, alpha=0.3)

    
    ax_psum = axes[3].twinx()
    if df_aws0 is not None:
        if "PSUM" in df_aws0.columns:
            df_aws0.loc[:,'PCUMSUM_trange'] = df_aws0['PSUM'].cumsum()
            ax_psum.plot(df_aws0.time, df_aws0.PCUMSUM_trange, color=caws, lw=lw_aws, ls=ls_aws)
    ax_psum.plot(ds_aro0.time, ds_aro0.PCUMSUM, color=caro, lw=l_width, ls=ls_aro)
    ax_psum.plot(ds_ifs0.time, ds_ifs0.PCUMSUM, color=cifs, lw=l_width, ls=ls_ifs)

    axes[3].set_ylabel(r'P / mmh$^{-1}$')
    axes[3].set_ylim([-0.1,4]) # Precipitation in mm/h
    ax_psum.set_ylabel(r'P$_{sum,t_0}$ / mm')
    ax_psum.xaxis.set_minor_locator(AutoMinorLocator())
    ax_psum.yaxis.set_minor_locator(AutoMinorLocator())
    ax_psum.grid(False)
    
    """ISWR"""
    df_toa_solar_rad = toa_solar_radiation(station_info["latitude"],station_info["longitude"],sdate,edate)
    axes[4].plot(df_toa_solar_rad.index, df_toa_solar_rad['TOA'], color='gray', lw=l_width, ls='dotted', label='potential ISWR (TOA)')
    
    if df_aws is not None:
        axes[4].plot(df_aws0.time, df_aws0.ISWR, color=caws, lw=lw_aws, ls=ls_aws)
    axes[4].plot(ds_aro0.time, ds_aro0.ISWR, color=caro, lw=l_width, ls=ls_aro)
    axes[4].plot(ds_ifs0.time, ds_ifs0.ISWR, color=cifs, lw=l_width, ls=ls_ifs)
    axes[4].legend(loc='upper right', ncol=1,  framealpha=1, edgecolor='black')

    # # start = '2021-01-01 00:00:00'
    # start_toa = pd.to_datetime(ds_nwp.time[0].values.astype(str))
    # start_toa = start_toa.strftime('%Y-%m-%d %H:%M:%S')
    # end_toa = pd.to_datetime(ds_nwp.time[-1].values.astype(str))
    # end_toa = end_toa.strftime('%Y-%m-%d %H:%M:%S')
    # df_toa_solar_rad = pro_inca.toa_solar_radiation(LAT,LON,start_toa,end_toa)
    # axes[4].plot(df_toa_solar_rad.index, df_toa_solar_rad['TOA'], color='gray', lw=l_width, ls='dotted', label='potential ISWR (TOA)')
    axes[4].set_ylabel(r'ISWR / Wm$^{-2}$')
    # axes[4].legend(loc='upper right')
    axes[4].set_ylim([-10,790])

    """ILWR"""
    axes[5].plot(ds_aro0.time, ds_aro0.ILWR, color=caro, lw=l_width, ls=ls_aro)
    axes[5].plot(ds_ifs0.time, ds_ifs0.ILWR, color=cifs, lw=l_width, ls=ls_ifs)
    axes[5].set_ylabel(r'ILWR / Wm$^{-2}$')
    axes[5].set_ylim([140,360])

    """Labels, legend, formatting"""
    axes[0].set_xlim([pd.to_datetime(sdate),pd.to_datetime(edate)])

    t=pd.to_datetime(str(ds_ifs0.time[0].values)) 
    start_date = t.strftime('%Y-%b-%d')
    axes[0].text(0.02, 0.935, 'Station: ' + station_info['name'], transform=axes[0].transAxes, verticalalignment='top', bbox={"boxstyle" : "round", "lw":0.67, "facecolor":"white", "edgecolor":"black"})
    axes[0].text(0.2, 0.935, sdate, transform=axes[0].transAxes, verticalalignment='top', bbox={"boxstyle" : "round", "lw":0.67, "facecolor":"white", "edgecolor":"black"})
    # axes[0].set_xlabel('Time ({} + 14d)'.format(start_date))
    #ax0.ticklabel_format(axis='x', style='sci', scilimits=(-2,2))
    
    for ax in axes:
        if ax==axes[2] or ax==axes[3]:
            ax.xaxis.set_minor_locator(AutoMinorLocator())
            ax.yaxis.set_minor_locator(AutoMinorLocator())

            ax.tick_params(which='both', width=1.2, top=True)
        else:
            ax.xaxis.set_minor_locator(AutoMinorLocator())
            ax.yaxis.set_minor_locator(AutoMinorLocator())

            ax.tick_params(which='both', width=1.2, top=True, right=True)

            if ax!=axes[5]: # ISWR
                symb_ax = '-'
                col_ax = 'lightgrey'
                lw_ax = 1
                ax.axhline(y=0, xmin=0, xmax=1, lw=lw_ax,ls=symb_ax,color=col_ax)
        ax.grid()

    date_form = mdates.DateFormatter("%b-%d")
    axes[0].xaxis.set_major_formatter(date_form)

    # --- Text --- #
    folder = 'output'
    fig_title = 'Timeseries-' + station_info['name'] + '-' + start_date + '.png'

    # --- Save figure --- #
    fig.tight_layout()
    fig.savefig(os.path.join(folder,fig_title), facecolor='w', edgecolor='w',
                    format='png', dpi=200)
    

def linear_func(x, m, c):
    return m * x + c

def linear_func2(x, m):
    return m * x

def plot_linear_fit_for_scatter_plot(var_aws,var_nwp,ax,index_nwp,cnwp):
    """Linear fit and correlation coefficients"""
    idx = np.isfinite(var_aws) & np.isfinite(var_nwp)
    slope_m, y_intercept, r_value, p_value, std_err = stats.linregress(var_aws[idx], var_nwp[idx])
    r_squared = r_value**2
    x_fit = np.linspace(np.nanmin(var_aws), np.nanmax(var_aws),np.sum(idx))
    y_fit = slope_m * x_fit + y_intercept
    
    ##### SECOND OPTION #####
    # params, params_covariance = optimize.curve_fit(linear_func, var_aws[idx], var_nwp[idx])
    # slope_m     = params[0]
    # y_intercept = params[1]
    # x_fit = var_aws[idx]
    # y_fit = linear_func(x_fit, slope_m, y_intercept)

    # residuals = var_nwp[idx] - y_fit
    # ss_residual = np.sum(residuals**2)
    # ss_total = np.sum((var_nwp[idx] - np.mean(var_nwp[idx]))**2)
    # r_squared = 1 - (ss_residual / ss_total)
    ##### SECOND OPTION #####

    #fit, _, _, _ = np.linalg.lstsq(var_aws[:,np.newaxis], var_nwp)
    #slope     = fit[0]
    #intercept = fit[1]

    #lab = 'm: ' + str(round(slope,2)) + '\nR: ' + str(round(r_value,2))
    if y_intercept < 0:
        lab = r'R$^2$: ' + str(round(r_squared,2)) + '\ny = ' + str(round(slope_m,2)) + 'x - ' + str(abs(round(y_intercept,2)))
    else:
        lab = r'R$^2$: ' + str(round(r_squared,2)) + '\ny = ' + str(round(slope_m,2)) + 'x + ' + str(round(y_intercept,2))
    
    ax.plot(x_fit, y_fit, lw=2.5, ls='--', c=cnwp)
    #ax.plot(x_fit2, y_fit2, lw=2, ls='--', c=cnwp)

    yp = 0.05 + index_nwp*0.17
    ax.text(0.95, yp, lab, transform=ax.transAxes, horizontalalignment='right', verticalalignment='bottom', color=cnwp, bbox={"boxstyle" : "round", "lw":0.67, "facecolor":"white", "edgecolor":"black"})
    

def scatter_plots_nwp_aws(nwp_models: list, station_info: dict, df_aws: object, sdate, edate):
    "Compare NWP data to other NWP data or to AWS data. Up to three "

    gskw = {'hspace':0.2,'wspace':0.2}
    fig, axes = plt.subplots(3,2,figsize=(8,12), gridspec_kw=gskw)
    axes = axes.flatten()

    edate_dt      = datetime.strptime(edate, "%Y-%m-%d")
    edate_dt_sel  = edate_dt - timedelta(days=1)
    edate_sel     = edate_dt_sel.strftime("%Y-%m-%d")

    caro = 'forestgreen'
    cifs = 'blueviolet'
    nwp_colors = [cifs, caro]
    nwp_labels = ['IFS', 'AROME']
    
    
    MARK  = 'o' # 'o', 'D', '$0$'
    SMARK = 7                         
    al    = 1
    mlw   = 0.3

    qq_cmap = plt.get_cmap('plasma') # # Colormap('plasma', N=len(ds_aws_map.time))
    # CLEV = [0,2,4,6,8,10]
    CLEV = [0,1,2,3,4,5,6,7,8,9,10]
    # CLEV = np.lisnpace(0,lead_time,10)
    cnorm = BoundaryNorm(boundaries=CLEV, ncolors=qq_cmap.N, clip=True) 

    for i, ds_nwp in enumerate(nwp_models):
        """Prepare data / Select location"""
        cnwp = nwp_colors[i]

        ds_nwp0 = ds_nwp.sel(x=station_info['easting'],y=station_info['northing'],method='nearest')
        ds_nwp0 = ds_nwp0.sel(time=slice(sdate,edate_sel))
    
        """Filter AWS data with NWP timestamps data (hourly data)"""
        if "PSUM" in df_aws.columns:
            wbar = df_aws.time[10]-df_aws.time[9]
            tres = wbar.seconds // 60
            df_aws['PSUM_scaled'] = df_aws['PSUM'].rolling(window=int(60/tres)).sum()
        
        df_aws0 = df_aws[df_aws['time'].isin(ds_nwp0.time.values)]
        
        """Scatter plots"""
        ta_thres = 100
        idx = np.where(df_aws0['TA'].values < ta_thres, True, False)
        scat_plot = axes[0].scatter(df_aws0['TA'], ds_nwp0['TA'], alpha=al, s=SMARK, marker=MARK, edgecolor=cnwp, facecolor='None', linewidths=mlw, label=nwp_labels[i])
        plot_linear_fit_for_scatter_plot(df_aws0['TA'].values[idx],ds_nwp0['TA'].values[idx],axes[0],i,cnwp)
        legend = axes[0].legend(loc='upper left', markerscale=2)
        for legend_handle in legend.legendHandles:
            legend_handle.set_linewidth(1.5)

        axes[1].scatter(df_aws0['RH'], ds_nwp0['RH'], s=SMARK, marker=MARK, edgecolor=cnwp, facecolor='None', linewidths=mlw)
        plot_linear_fit_for_scatter_plot(df_aws0['RH'].values,ds_nwp0['RH'].values,axes[1],i,cnwp)

        if "PSUM_scaled" in df_aws0.columns:
            psum_thres = 0.0
            idx = np.where(df_aws0['PSUM_scaled'].values >= psum_thres, True, False)
            axes[2].scatter(df_aws0['PSUM_scaled'], ds_nwp0['PSUM'], s=SMARK, marker=MARK, edgecolor=cnwp, facecolor='None', linewidths=mlw)
            plot_linear_fit_for_scatter_plot(df_aws0['PSUM_scaled'].values[idx],ds_nwp0['PSUM'].values[idx],axes[2],i,cnwp)

        vw_thres = 0.5
        idx = np.where(df_aws0['VW'].values > vw_thres, True, False)
        axes[3].scatter(df_aws0['VW'], ds_nwp0['VW'], s=SMARK, marker=MARK, edgecolor=cnwp, facecolor='None', linewidths=mlw)
        plot_linear_fit_for_scatter_plot(df_aws0['VW'].values[idx],ds_nwp0['VW'].values[idx],axes[3],i,cnwp)
        #plot_linear_fit_for_scatter_plot(ds_nwp0['VW'].values,df_aws0['VW'].values,axes[3],i,cnwp)
        
        #print(df_aws0['VW'].values)  
        #print(ds_nwp0['VW'].values)
        # ds_aws_map['WR_DELTA_temp'] = ds_nwp['WR']-ds_aws_map['WR']
        # ds_aws_map['WR_DELTA_tem'] = np.where(ds_aws_map['WR_DELTA_temp']<-180,360+ds_aws_map['WR_DELTA_temp'],ds_aws_map['WR_DELTA_temp'])
        # ds_aws_map['WR_DELTA'] = np.where(ds_aws_map['WR_DELTA_tem']>180,ds_aws_map['WR_DELTA_tem']-360,ds_aws_map['WR_DELTA_tem'])
        # ds_aws_map['WR_DELTA'] = np.where(ds_aws_map['WG']>5,ds_aws_map['WR_DELTA'],np.nan)
        # axes[3].scatter(ds_aws_map['WR'], ds_aws_map['WR_DELTA'], alpha=0.6, s=SMARK, marker=MARK, c=cmap_vector, cmap=qq_cmap, norm=cnorm, linewidths=mlw)

        iswr_thres = 10
        idx = np.where(df_aws0['ISWR'].values > iswr_thres, True, False)
        axes[4].scatter(df_aws0['ISWR'], ds_nwp0['ISWR'], s=SMARK, marker=MARK, edgecolor=cnwp, facecolor='None', linewidths=mlw)
        plot_linear_fit_for_scatter_plot(df_aws0['ISWR'].values[idx],ds_nwp0['ISWR'].values[idx],axes[4],i,cnwp)

    """Figure formatting"""
    #scat_plot.set_facecolor('none')
    #scat_plot.set_edgecolor()    
    for i, ax in enumerate(axes):
        if i != 5:
            ax.axhline(y=0, color='grey')
            ax.axvline(x=0, color='grey')

        ax.xaxis.set_minor_locator(AutoMinorLocator())
        ax.yaxis.set_minor_locator(AutoMinorLocator())

        ax.axis('scaled')
        ax.grid()

    TA_lims = [-21,12]
    RH_lims = [20,100]
    VW_lims = [0,19]
    SW_lims = [0,930]
    PSUM_lims = [0,4]
    line_45_s = '-.'
    line_45 = np.linspace(TA_lims[0],TA_lims[1],50)
    axes[0].plot(line_45,line_45, color='grey', linestyle=line_45_s)
    line_45 = np.linspace(RH_lims[0],RH_lims[1],50)
    axes[1].plot(line_45,line_45, color='grey', linestyle=line_45_s)
    line_45 = np.linspace(PSUM_lims[0],PSUM_lims[1],50)
    axes[2].plot(line_45,line_45, color='grey', linestyle=line_45_s)
    line_45 = np.linspace(VW_lims[0],VW_lims[1],50)
    axes[3].plot(line_45,line_45, color='grey', linestyle=line_45_s)
    line_45 = np.linspace(SW_lims[0],SW_lims[1],50)
    axes[4].plot(line_45,line_45, color='grey', linestyle=line_45_s)
        
    #axes[0].set_xlim(TA_lims)
    #axes[0].set_ylim(TA_lims)
    axes[1].set_xlim(RH_lims)
    axes[1].set_ylim(RH_lims)
    axes[2].set_xlim(PSUM_lims)
    axes[2].set_ylim(PSUM_lims)
    axes[3].set_xlim(VW_lims)
    axes[3].set_ylim(VW_lims)
    axes[4].set_xlim(SW_lims)
    axes[4].set_ylim(SW_lims)
    
    axes[0].set_xticks([-20,-15,-10,-5,0,5,10])
    axes[0].set_yticks([-20,-15,-10,-5,0,5,10])
    axes[1].set_xticks([20,40,60,80,100])
    axes[1].set_yticks([20,40,60,80,100])
    axes[2].set_xticks([0,1,2,3,4])
    axes[2].set_yticks([0,1,2,3,4])
    axes[3].set_xticks([0,5,10,15])
    axes[3].set_yticks([0,5,10,15])
    axes[4].set_xticks([0,200,400,600,800])
    axes[4].set_yticks([0,200,400,600,800])

    axes[0].set_xlabel("Air temperature / °C (AWS)")
    axes[0].set_ylabel("Air temperature / °C (NWP)")
    axes[1].set_xlabel("Relative humidity / % (AWS)")
    axes[1].set_ylabel("Relative humidity / % (NWP)")
    axes[2].set_xlabel(r"Precipitation / mmh$^{-1}$ (AWS)")
    axes[2].set_ylabel(r"Precipitation / mmh$^{-1}$ (NWP)")
    axes[3].set_xlabel(r"Wind / ms$^{-1}$ (AWS)")
    axes[3].set_ylabel(r"Wind / ms$^{-1}$ (NWP)")
    axes[4].set_xlabel(r"ISWR / Wm$^{-2}$ (AWS)")
    axes[4].set_ylabel(r"ISWR / Wm$^{-2}$ (NWP)")

    axes[5].axis('off')

    # fig.text(0.47,0.68,center_str,horizontalalignment='center',
    #         verticalalignment='center', ma='center', fontsize=10)

    """Save figure"""
    t          = pd.to_datetime(str(ds_nwp.time[0].values)) 
    start_date = t.strftime('%Y-%b-%d')
    folder     = 'output'
    fig_title  = 'Scatter-plot-' + station_info['name'] + '-' + start_date + '.png'

    # fig.tight_layout()
    fig.savefig(os.path.join(folder,fig_title), facecolor='w', edgecolor='w', bbox_inches='tight', format='png', dpi=200)
    

class TaylorDiagram(object):
  def __init__(self, STD ,fig=None, rect=111, label='_'):
    self.STD = STD
    tr = PolarAxes.PolarTransform()
    ## Correlation labels
    rlocs = np.concatenate(((np.arange(11.0) / 10.0), [0.95, 0.99]))
    tlocs = np.arccos(rlocs) # Conversion to polar angles
    gl1 = gf.FixedLocator(tlocs) # Positions
    tf1 = gf.DictFormatter(dict(zip(tlocs, map(str, rlocs))))
    ## Standard deviation axis extent
    self.smin = 0
    self.smax = 1.6 * self.STD
    gh = fa.GridHelperCurveLinear(tr,extremes=(0,(np.pi/2),self.smin,self.smax),grid_locator1=gl1,tick_formatter1=tf1,)
    if fig is None:
      fig = plt.figure()
    ax = fa.FloatingSubplot(fig, rect, grid_helper=gh)
    fig.add_subplot(ax)
    ## Angle axis
    ax.axis['top'].set_axis_direction('bottom')
    ax.axis['top'].label.set_text("Correlation coefficient")
    ax.axis['top'].toggle(ticklabels=True, label=True)
    ax.axis['top'].major_ticklabels.set_axis_direction('top')
    ax.axis['top'].label.set_axis_direction('top')
    # X axis
    ax.axis['left'].set_axis_direction('bottom')
    ax.axis['left'].label.set_text("Relative standard deviation")
    ax.axis['left'].toggle(ticklabels=True, label=True)
    ax.axis['left'].major_ticklabels.set_axis_direction('bottom')
    ax.axis['left'].label.set_axis_direction('bottom')
    # Y axis
    ax.axis['right'].set_axis_direction('top')
    ax.axis['right'].label.set_text("Relative standard deviation")
    ax.axis['right'].toggle(ticklabels=True, label=True)
    ax.axis['right'].major_ticklabels.set_axis_direction('left')
    ax.axis['right'].label.set_axis_direction('top')
    ## Useless
    ax.axis['bottom'].set_visible(False)
    ## Contours along standard deviations
    ax.grid()
    self._ax = ax # Graphical axes
    self.ax = ax.get_aux_axes(tr) # Polar coordinates
    ## Add reference point and STD contour
    l ,  = self.ax.plot([0], self.STD, 'k*', ls='', ms=12, label=label)
    l1 , = self.ax.plot([0], self.STD, 'k*', ls='', ms=12, label=label)
    t    = np.linspace(0, (np.pi / 2.0))
    t1   = np.linspace(0, (np.pi / 2.0))
    r    = np.zeros_like(t) + self.STD
    r1   = np.zeros_like(t) + self.STD
    self.ax.plot(t, r, 'k--', label='_')
    # Collect sample points for latter use (e.g. legend)
    self.samplePoints = [l]
    self.samplePoints = [l1]
    
  def add_sample_scatter(self,STD,r,*args,**kwargs):
    #l,= self.ax.plot(np.arccos(r), STD, *args, **kwargs) # (theta, radius)
    l = self.ax.scatter(np.arccos(r), STD, *args, **kwargs) # (theta, radius)
    self.samplePoints.append(l)
    return l

  def add_sample(self,STD,r,*args,**kwargs):
    l,= self.ax.plot(np.arccos(r), STD, *args, **kwargs) # (theta, radius)
    self.samplePoints.append(l)
    return l

  def add_contours(self,levels=5,**kwargs):
    """Add contours of RMSE which is calculated from R and STD"""
    rs, ts = np.meshgrid(np.linspace(self.smin, self.smax), np.linspace(0, (np.pi / 2.0)))
    RMSE=np.sqrt(np.power(self.STD, 2) + np.power(rs, 2) - (2.0 * self.STD * rs  *np.cos(ts)))
    contours = self.ax.contour(ts, rs, RMSE, levels, **kwargs)
    return contours
  


################
def toa_solar_radiation(lat,lon,start,end,res='10min'):
    """
    Calculates a timeseries TOA (potential) solar radiation for a certain location
    Note that actual global radiation measured at one point depends on factors like the following:
    atmospheric transmission (clouds, aerosols, ...)
    topographic shading
    sources of diffuse radiation (reflections from surrounding terrain, clouds, ...) 
    """
    # alt = 2825        # altitude of reference location
    lsm = 15            # reference meridian of timezone (central europe = 15°)
     # accepted input for resolution res... formats: '1h', '10min', '4s', etc...

    start_year = int(start[0:4])
    end_year = int(end[0:4])
    years = np.arange(start_year, end_year+1)
    for year in years:
        d1 = str(year) + '-01-01'
        d2 = str(year+1) + '-01-01'
        date = pd.date_range(d1,d2,freq=res)
        # gamma is 0 for yyyy-01-01 00:00:00 and is 2*pi for yyyy-12-31 24:00:00
        gamma = np.linspace(0,2*np.pi,len(date))
        if year == years[0]:
            df = pd.DataFrame(gamma,columns=['gamma'],index=date)[:-1]
        else:
            df_temp = pd.DataFrame(gamma,columns=['gamma'],index=date)
            df = df.append(df_temp[:-1])

    # - Get requested period - #
    df = df.loc[start:end]

    # - Vector for local standard time - #
    LST = df.index.hour + df.index.minute / 60 + df.index.second / 3600
    gamma = df['gamma']

    # 1B: Eccentricity of earth...E0 [dimensionless]
    E0 = 1.000110+0.034221*np.cos(gamma)+0.001280*np.sin(gamma)+0.000719*np.cos(2*gamma)+0.000077*np.sin(2*gamma)
    df['excentricity'] = E0

    # 1C: declination...delta [rad]
    delta = (0.006918-0.399912*np.cos(gamma)+0.070257*np.sin(gamma)-0.006758*np.cos(2*gamma)+0.000907*np.sin(2*gamma)-0.002697*np.cos(3*gamma)+0.00148*np.sin(3*gamma))
    df['declination'] = delta

    # 1D: Equation of time (Et) and local apparent time (LAT)...[h]   
    Et = ((0.000075 + 0.001868*np.cos(gamma) - 0.032077*np.sin(gamma)
        - 0.014615*np.cos(2*gamma) - 0.04089*np.sin(2*gamma))
            *229.18/60) 
    LAT = LST + Et + 4/60*(lon-lsm)-6/10*lon/lsm #[h] Iqbal(1.4.2)
    LAT = np.where(LAT<0,LAT+24,LAT) # avoid negative values of local apparent time (-0.5h = 23.5h)

    # meanshift = 10/4; %data storage (here means over 10 minutes) 
    # omega = omega - meanshift
    df['time_equation'] = Et
    df['LAT'] = LAT

    # 1E: Hour angle...omega
    #  south is 0, east negative, west positive...[rad]           (Sproul(7))
    omega = (LAT-12)*np.deg2rad(15)
    df['hour_angle'] = omega

    # 1F: zenith angle...z and elevation angle...sunel of sun at each timestep 
    latrad = np.deg2rad(lat)
    sunel = np.arcsin(np.sin(latrad)*np.sin(delta) + np.cos(latrad)*np.cos(delta)*np.cos(omega))
    # Not the same equation as in Iqubal because of different omega!
    z = np.pi/2-sunel
    df['zenith_angle'] = z
    df['sun_elevation'] = sunel

    TOA = 1368*np.cos(z)
    df['TOA'] = np.where(TOA<0,0,TOA)
    # df['TOA'].plot(figsize=(15,8))
    return df


def visualize_profile_location(ds,lat,lon,xx,yy,data_crs,alt_prof,alt_model,SIM_FOLDER):
    """
    """
    # - Import EAWS micro regions (polygon regions) - #  
    regions = []
    directory = '../eaws-regions/public/micro-regions/'
    for entry in os.scandir(directory):
        if entry.path.endswith(".json") and entry.is_file() and ('AT-07' in entry.path or 'IT-32-BZ' in entry.path):
            with open(entry.path, 'r') as f:
                file = json.load(f)

            for feat in file['features']:
                #region_id = ex_region_id(feat)
                # print(shape(feat['properties']))
                regions.append(shape(feat['geometry']))
    
    fig, ax = plt.subplots(1,1,figsize=(14,6),subplot_kw={'projection':ccrs.PlateCarree()})
    
    # ax = plt.axes(projection=ccrs.PlateCarree())
    contf = ax.contourf(ds.lon, ds.lat,ds.HGT_surface[0,:,:], cmap='terrain')
    # transform=data_crs
    cbar = plt.colorbar(contf, orientation='vertical',shrink=0.6)
    cbar.set_label('Altitude / m')
    # plt.colorbar(kmp, cmap=cmap,  ticks=bounds, ax=ax, orientation='vertical')
    # ax.axis('on')
    ax.set_aspect('equal')
    ax.gridlines(draw_labels=True)
    
    # - Plot EAWS micro regions - #
    for mpoli in regions:
        Polygons = list(mpoli.geoms)
        for poly in Polygons:
            x,y = poly.exterior.xy
            ax.plot(x,y, color = 'dimgray',lw=2,alpha=1)
    
        
    # - Visualize coordinates and verify transformation - #
    shift1 = 0.01
    shift2 = 0.05
    shift3 = 0.11
    ax.scatter(x=lon,y=lat,marker='o', color='firebrick',s=150, label='LAT/LON')
    ax.scatter(x=xx,y=yy,marker='x', s = 250, color='purple',lw=3, transform=data_crs, label='NORTHING/EASTING')
    ax.text(x=lon+shift1,y=lat+shift3,s='Elevation Model:  ' + str(alt_model) + 'm', fontweight='heavy', fontsize='large', color='black')
    ax.text(x=lon+shift1,y=lat+shift2,s='Elevation Profile: ' + str(int(alt_prof)) + 'm', fontweight='heavy', fontsize='large', color='black')
    
    ax.legend()

    # --- Save fig --- #
    # Check FC data folder
    if not os.path.exists(SIM_FOLDER + '/Figures'):
        # Create a new directory for FC data
        os.makedirs(SIM_FOLDER + '/Figures')
        print("New directory for Figures created..")

    fig_name = 'snowprof-location.png'
    # fig.tight_layout()
    fig.savefig(SIM_FOLDER + '/Figures/' + fig_name, facecolor='w', edgecolor='w',
                format='png', dpi=150) # orientation='portrait'
 

def visualize_precipitation_sum(ds,lat,lon,cumsum_precip,SIM_FOLDER):
    """
    """
    fig, ax = plt.subplots(1,1,figsize=(14,6))
    label_cumsum = 'Lat: ' + str(lat) + ' Lon: ' + str(lon)
    ax.plot(ds.time, cumsum_precip, color='peru', lw=2, label=label_cumsum)
        
    # - Visualize coordinates and verify transformation - #
    shift1 = 0.01
    shift2 = 0.05
    shift3 = 0.11
    # ax.scatter(x=lon,y=lat,marker='o', color='firebrick',s=150, label='LAT/LON')
    # ax.scatter(x=xx,y=yy,marker='x', s = 250, color='purple',lw=3, transform=data_crs, label='NORTHING/EASTING')
    # ax.text(x=lon+shift1,y=lat+shift3,s='Elevation Model:  ' + str(alt_model) + 'm', fontweight='heavy', fontsize='large', color='black')
    # ax.text(x=lon+shift1,y=lat+shift2,s='Elevation Profile: ' + str(int(alt_prof)) + 'm', fontweight='heavy', fontsize='large', color='black')
    
    ax.legend()
    ax.set_ylabel(r'P$_{sum,t_0}$ / mm')
    ax.set_xlabel('Time')

    # - Format figure - #
    ax.xaxis.set_minor_locator(AutoMinorLocator())
    ax.yaxis.set_minor_locator(AutoMinorLocator())
    ax.tick_params(which='both', width=1.2, top=True, right=True)

    symb_ax = '-'
    col_ax = 'lightgrey'
    lw_ax = 1
    # ax.axhline(y=0, xmin=0, xmax=1, lw=lw_ax,ls=symb_ax,color=col_ax)

    date_form = mdates.DateFormatter("%b-%d")
    ax.xaxis.set_major_formatter(date_form)

    # --- Save fig --- #
    # Check FC data folder
    if not os.path.exists(SIM_FOLDER + '/Figures'):
        # Create a new directory for FC data
        os.makedirs(SIM_FOLDER + '/Figures')
        print("New directory for Figures created..")

    fig_name = 'cumulative-precipitation.png'
    # fig.tight_layout()
    fig.savefig(SIM_FOLDER + '/Figures/' + fig_name, facecolor='w', edgecolor='w',
                format='png', dpi=150) # orientation='portrait'


def visualize_5day_precipitation_Tirol(ds,lat,lon,SIM_FOLDER):
    """
    """
# - Import EAWS micro regions (polygon regions) - #  
    regions = []
    directory = '../eaws-regions/public/micro-regions/'
    for entry in os.scandir(directory):
        if entry.path.endswith(".json") and entry.is_file() and ('AT-07' in entry.path or 'IT-32-BZ' in entry.path):
            with open(entry.path, 'r') as f:
                file = json.load(f)

            for feat in file['features']:
                #region_id = ex_region_id(feat)
                # print(shape(feat['properties']))
                regions.append(shape(feat['geometry']))
    
    fig, ax = plt.subplots(1,1,figsize=(14,6),subplot_kw={'projection':ccrs.PlateCarree()})
    
    # - PRECIPITATION SUM - #
    precip_5day_Tirol = ds['PSUM'][0:120,:,:].sum(axis=0)

    # ax = plt.axes(projection=ccrs.PlateCarree())
    contf = ax.contourf(ds.lon, ds.lat,precip_5day_Tirol, cmap='plasma_r')
    # transform=data_crs
    cbar = plt.colorbar(contf, orientation='vertical',shrink=0.6)
    cbar.set_label('precipitation / mm')
    # plt.colorbar(kmp, cmap=cmap,  ticks=bounds, ax=ax, orientation='vertical')
    # ax.axis('on')
    ax.set_aspect('equal')
    ax.gridlines(draw_labels=True)
    
    # - Plot EAWS micro regions - #
    for mpoli in regions:
        Polygons = list(mpoli.geoms)
        for poly in Polygons:
            x,y = poly.exterior.xy
            ax.plot(x,y, color = 'dimgray',lw=2,alpha=1)
    
        
    # - Visualize coordinates and verify transformation - #
    ax.scatter(x=lon,y=lat,marker='x', color='black',s=150)

    # --- Save fig --- #
    # Check FC data folder
    if not os.path.exists(SIM_FOLDER + '/Figures'):
        # Create a new directory for FC data
        os.makedirs(SIM_FOLDER + '/Figures')
        print("New directory for Figures created..")

    fig_name = '5day-precipitation-Tirol.png'
    # fig.tight_layout()
    fig.savefig(SIM_FOLDER + '/Figures/' + fig_name, facecolor='w', edgecolor='w',
                format='png', dpi=150) # orientation='portrait'
    
    return precip_5day_Tirol