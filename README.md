# NWP-model-analysis

A Python notebook for analysing NWP model data and comparing it to measurements from automatic weather stations (AWS).

<p float="center">
  <img src="./figures/Timeseries-AXLIZ-2023-Jan-30.png" width="85%" />
</p>

<p float="center">
  <img src="./figures/Scatter-plot-AXLIZ-2022-Nov-01.png" width="85%" />
</p>


## Taylor diagrams

Taylor diagrams display statistics useful for assessing the similarity of a variable simulated by a model (more generally, the “test” field) to its observed counterpart (more generally, the “reference” field). It is used to quantify the degree of correspondence between the modeled and observed behavior in terms of three statistics: the Pearson correlation coefficient, the root-mean-square error (RMSE) error, and the standard deviation. Mathematically, the three statistics displayed on a Taylor diagram are related by the error propagation formula (which can be derived directly from the definition of the statistics appearing in it):

{\displaystyle E^{\prime 2}=\sigma _{\Delta }^{2}=\sigma _{r}^{2}+\sigma _{t}^{2}-2\sigma _{r}\sigma _{t}\rho },

where ρ is the correlation coefficient between the test and reference fields, E′ is the centered RMS difference between the fields (with any difference in the means first removed), and \sigma _{r} and \sigma _{t} are the standard deviations of the reference and test fields, respectively. 

The means of the fields are subtracted out before computing their second-order statistics, so the diagram does not provide information about overall biases (mean error), but solely characterizes the centered pattern error.

By normalizing the standard deviation (dividing both the RMS difference and the standard deviation of the "test" field by the standard deviation of the observations) so that the "observed" point is plotted at unit distance from the origin along the x-axis, allows showing statistics for different fields (with different units) in a single plot.

Below different colors refer to different NWP models and each marker represents a different weather station.

<p float="center">
  <img src="./figures/taylor_diagram_2022-11-01_TA.png" width="45%" />
  <img src="./figures/taylor_diagram_2022-11-01_RH.png" width="45%" />
</p>